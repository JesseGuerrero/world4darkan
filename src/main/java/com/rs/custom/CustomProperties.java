package com.rs.custom;

import com.rs.Settings;
import com.rs.engine.command.Commands;
import com.rs.engine.dialogue.Dialogue;
import com.rs.engine.dialogue.HeadE;
import com.rs.engine.dialogue.Options;
import com.rs.engine.quest.Quest;
import com.rs.game.content.Toolbelt;
import com.rs.game.content.combat.CombatDefinitions;
import com.rs.game.content.quests.shieldofarrav.ShieldOfArrav;
import com.rs.game.content.skills.cooking.Cooking;
import com.rs.game.content.skills.cooking.CookingD;
import com.rs.game.content.skills.dungeoneering.DungeonConstants;
import com.rs.game.content.skills.dungeoneering.DungeonManager;
import com.rs.game.content.skills.dungeoneering.RoomNode;
import com.rs.game.content.skills.farming.FarmPatch;
import com.rs.game.content.skills.firemaking.Bonfire;
import com.rs.game.content.skills.magic.LodestoneAction;
import com.rs.game.content.skills.smithing.ForgingInterface;
import com.rs.game.content.skills.smithing.Smithing;
import com.rs.game.model.entity.npc.combat.NPCCombatDefinitions;
import com.rs.game.model.entity.player.Player;
import com.rs.lib.Constants;
import com.rs.lib.game.Item;
import com.rs.lib.game.Rights;
import com.rs.plugin.annotations.PluginEventHandler;
import com.rs.plugin.annotations.ServerStartupEvent;
import com.rs.plugin.handlers.ItemClickHandler;
import com.rs.plugin.handlers.ItemOnObjectHandler;
import com.rs.plugin.handlers.LoginHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.rs.game.content.skills.slayer.ReaperAssignments.talkAboutAssignment;

@PluginEventHandler
public class CustomProperties {
	@ServerStartupEvent
	public static void init() {
		FarmPatch.FARMING_TICK = 350;//1.5x farming
	}

	public static int getDungDoorLevels(DungeonManager manager, DungeonConstants.SkillDoors sd, Random random) {
		return manager == null ? 1 : true ? (manager.getParty().getMaxLevel(sd.getSkillId()) - random.nextInt(10)) : random.nextInt(sd.getSkillId() == Constants.SUMMONING || sd.getSkillId() == Constants.PRAYER ? 100 : 106);
	}

	public static boolean hasRandoms() {
		return false;
	}

	public static LoginHandler onLoginUpdates = new LoginHandler(e-> {
		if(e.getPlayer().getI("death coffer") < 0)
			e.getPlayer().save("death coffer", 0);
		if((boolean)e.getPlayer().get("resetQuestsJuly") == false) {
//			if(!ShieldOfArrav.hasGang(e.getPlayer()))
			questsEnabled(e.getPlayer(), false);
			e.getPlayer().save("resetQuestsJuly", true);
		}
	});

	public static boolean makeClue1() {
		return true;
	}

	public static void questsEnabled(Player p, boolean enabled) {
		if (!enabled) {
			ShieldOfArrav.setGang(p, "Phoenix");
			if (!p.getQuestManager().completedAllQuests())
				for (Quest quest : Quest.values())
					if (quest.isImplemented()) {
						p.getQuestManager().setStage(quest, quest.getHandler().getCompletedStage());
						p.getQuestManager().sendQuestStage(quest);
						p.getQuestManager().sendQuestPoints();
					}
		}
	}

	public static int totalXPCap() {
		return 2_000_000_000;
	}

	public static boolean removeCorpStatReset() {
		return false;
	}

	public static int getLargeDungPartSizeRequirement() {
		return 1;
	}

	public static double[] dungCombatLevelsByComplexity() {
		return new double[]{0.25, 0.30, 0.40, 0.50, 0.60, 0.70};
	}

	public static int getDungBossMultiplier() {
		return 1;
	}

	public static int dungTokenMultiplier() {
		return 5;
	}

	/**
	 * in givestarter
	 * @param p
	 */
	public static void giveToolsLoadestones(Player p) {
		for (LodestoneAction.Lodestone stone : LodestoneAction.Lodestone.values()) {
			p.unlockLodestone(stone, null);
		}
		p.addToolbelt(1265);
		for (Toolbelt.Tools tool : Toolbelt.Tools.values()) {
			if (tool == null)
				continue;
			if (p.getToolbelt().get(tool) != null)
				continue;
			p.getToolbelt().put(tool, 1);
		}
		Toolbelt.refreshToolbelt(p);
	}

	public static void customCommands() {
		Commands.add(Rights.PLAYER, "toggleperk", "toggle different perks", (p, args) -> {

		});

		Commands.add(Rights.PLAYER, "allperkson", "Toggles all perks", (p, args) -> {
			p.sendMessage("All perks toggled on");
			p.save(Perks.MASTER, true);
		});

		Commands.add(Rights.PLAYER, "allperksoff", "Toggles all perks", (p, args) -> {
			p.sendMessage("All perks toggled off");
			p.save(Perks.MASTER, false);
		});


		Commands.add(Rights.PLAYER, "resetperks", "Resets all perks", (p, args) -> {

		});

	}

	public static NPCCombatDefinitions weakenNPCById(int id, NPCCombatDefinitions defs) {//combatLevel property must be public
		int[] npcIdsToWeaken = {15507, 15506};
		for(int npcId : npcIdsToWeaken)
			if(id == npcId)
				for(NPCCombatDefinitions.Skill skill : defs.combatLevels.keySet())
					defs.combatLevels.put(skill, (int) (defs.combatLevels.get(skill) * 0.7));
		return defs;
	}

	public static NPCCombatDefinitions weakenNPCByName(String name, NPCCombatDefinitions defs) {
		String[] npcNamesToWeaken = {"Queen Black Dragon", "Nex", "Kree'arra", "Corporeal Beast", "TzTok-Jad", "TokHaar-Jad", "Har-Aken",
				"Har-Aken (Ranged Tentacle)", "Har-Aken (Magic Tentacle)", "Dagannoth Rex", "Dagannoth Prime", "Dagannoth Supreme", "King Black Dragon"};
		for(String npcName : npcNamesToWeaken)
			if(name.equalsIgnoreCase(npcName))
				for(NPCCombatDefinitions.Skill skill : defs.combatLevels.keySet())
					defs.combatLevels.put(skill, (int) (defs.combatLevels.get(skill) * 0.7));
		return defs;
	}

	public static int weakenQBD() {
		return 7500/2;
	}

	public static int timesThree(int amount) {
		return amount * 3;
	}

	@ServerStartupEvent
	public static void checkNerfedLevels() {
		Map<NPCCombatDefinitions.Skill, Integer> levels = NPCCombatDefinitions.getDefs(8349).getLevels();
		for(int level : levels.values())
			if(level != 300)
				throw new RuntimeException("TD level is not 300");
	}

	public static boolean deathCofferIsSuccessful(Player player) {
//		if(player.isIronMan()) {
//			player.setNextTile(Settings.getConfig().getPlayerRespawnTile());
//			player.lock(3);
//			player.startConversation(new Dialogue().addNPC(15661, HeadE.CALM, "Given you have earned everything yourself(iron) you get a free pass...").addPlayer(HeadE.HAPPY_TALKING, "Thanks!"));
//			return true;
//		}
		if(player.getI("death coffer") < 10_000) {
			player.sendMessage("You didn't have enough in your coffer");
			return false;
		}
		if(player.getI("death coffer") >= 10_000) {
			player.reset();
			player.save("death coffer", player.getI("death coffer") - 10_000);
			player.setNextTile(Settings.getConfig().getPlayerRespawnTile());
			player.lock(3);
			player.startConversation(new Dialogue().addNPC(15661, HeadE.CALM, "This has cost you 10K coins").addPlayer(HeadE.HAPPY_TALKING, "Thanks!"));
			return true;
		}
		return false;
	}

	public static Options getDeathsOptions(Player player) {
		return new Options() {
			@Override
			public void create() {
				if (player.getInventory().getAmountOf(995) >= 10_000)
					option("I would like to add to my death coffer...", new Dialogue().addOptions("Choose an option:", new Options() {
						@Override
						public void create() {
							option("10k", new Dialogue()
									.addPlayer(HeadE.HAPPY_TALKING, "I would like to add 10k.")
									.addNext(() -> {
										player.getInventory().deleteItem(995, 10_000);
										player.save("death coffer", player.getI("death coffer") + 10_000);
									})
							);
							if (player.getInventory().getAmountOf(995) >= 100_000)
								option("100k", new Dialogue()
										.addPlayer(HeadE.HAPPY_TALKING, "I would like to add 100k.")
										.addNext(() -> {
											player.getInventory().deleteItem(995, 100_000);
											player.save("death coffer", player.getI("death coffer") + 100_000);
										})
								);
							if (player.getInventory().getAmountOf(995) >= 1_000_000)
								option("1M", new Dialogue()
										.addPlayer(HeadE.HAPPY_TALKING, "I would like to add 1M.")
										.addNext(() -> {
											player.getInventory().deleteItem(995, 1_000_000);
											player.save("death coffer", player.getI("death coffer") + 1_000_000);
										})
								);
							if (player.getInventory().getAmountOf(995) >= 10_000_000)
								option("10M", new Dialogue()
										.addPlayer(HeadE.HAPPY_TALKING, "I would like to add 10M.")
										.addNext(() -> {
											player.getInventory().deleteItem(995, 10_000_000);
											player.save("death coffer", player.getI("death coffer") + 10_000_000);
										})
								);
						}
					}));
				option("How much is in my death coffer?", new Dialogue()
						.addNPC(15661, HeadE.CALM, "Speak to me about my death coffers once you have at least 10k coins, however in your coffer " +
								"you currently have " + player.getI("death coffer") + " coins...")
				);
			}
		};
	}
}
