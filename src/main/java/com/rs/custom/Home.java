package com.rs.custom;

import com.rs.cache.loaders.ObjectType;
import com.rs.engine.dialogue.Dialogue;
import com.rs.engine.dialogue.Options;
import com.rs.game.World;
import com.rs.game.content.combat.CombatDefinitions.Spellbook;
import com.rs.game.content.world.areas.wilderness.WildernessController;
import com.rs.game.model.entity.npc.NPC;
import com.rs.game.model.entity.pathing.Direction;
import com.rs.game.model.object.GameObject;
import com.rs.lib.Constants;
import com.rs.lib.game.Tile;
import com.rs.lib.game.WorldObject;
import com.rs.plugin.annotations.PluginEventHandler;
import com.rs.plugin.annotations.ServerStartupEvent;
import com.rs.plugin.handlers.LoginHandler;
import com.rs.plugin.handlers.NPCClickHandler;
import com.rs.plugin.handlers.ObjectClickHandler;

@PluginEventHandler
public class Home {
	
	@ServerStartupEvent
	public static void spawnNPCs() {
		spawnNPC(15661, Tile.of(2884, 3530, 0), Direction.SOUTH, false);
		/* GIM */
		spawnNPC(1512, Tile.of(2887, 3530, 0), "GIM Tutor", Direction.SOUTH, false);
		World.unclipTile(Tile.of(3165, 3482, 0));
	}

	public static LoginHandler loginBlockThings = new LoginHandler(e -> {
		if(e.getPlayer().getBool("Group IronMan"))
			if(e.getPlayer().getO("GIM Team") == null) {
				e.getPlayer().sendMessage("<col=FF0000><shad=000000>You are XP locked until you are in a team...");
				e.getPlayer().setXpLocked(true);
			}
		if(e.getPlayer().getO("GIM Team") != null)
			e.getPlayer().setXpLocked(false);
	});



	public static NPC spawnNPC(int id, Tile tile, String name, Direction direction, boolean randomWalk) {
		NPC npc = new NPC(id, tile);
		if (name != null)
			npc.setPermName(name);
		if (direction != null)
			npc.setFaceAngle(direction.getAngle());
		npc.setRandomWalk(randomWalk);
		return npc;
	}
	
	public static NPC spawnNPC(int id, Tile tile, Direction direction, boolean randomWalk) {
		return spawnNPC(id, tile, null, direction, randomWalk);
	}
	
	public static NPC spawnNPC(int id, Tile tile, String name) {
		return spawnNPC(id, tile, name, null, false);
	}
	
	public static WorldObject spawnObject(int id, Tile tile) {
		GameObject object = new GameObject(id, ObjectType.SCENERY_INTERACT, 0, tile);
		World.spawnObject(object);
		return object;
	}
}
