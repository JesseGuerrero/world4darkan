package com.rs.custom;

import com.rs.game.content.Potions;
import com.rs.game.content.skills.fishing.Fish;
import com.rs.game.content.skills.fishing.Fishing;
import com.rs.game.content.skills.fishing.FishingSpot;
import com.rs.game.model.entity.player.Player;
import com.rs.lib.util.Utils;

import java.util.List;

public class Perks {

	public static boolean isMasterOn(Player player) {
		return player.getBool(MASTER);
	}

	public static final String
	MASTER = "Master",
	MELEE_CRITICAL_STRIKE = "Melee Boost",
	CONSTITUTION_DOUBLE_REGEN = "Constitution Boost",
	MAGIC_FREE_CAST = "Magic Boost",
	PRAYER_DOUBLE_BONES = "Prayer Boost",
	RANGE_BOOST_SPECIAL = "Range Boost",
	SLAYER_BOOST_POINTS = "Slayer Boost",
	SUMMONING_MORE_CHARMS_LESS_SHARD_USE = "Summoning Boost",
	AGILITY_REVISE = "Agility Boost",
	CONSTRUCTION_SAVE_PLANKS = "Construction Boost",
	COOKING_DOUBLE_COOK = "Cooking Boost",
	CRAFTING_EXTRA_ITEM_CHANCE_SOFT_CLAY = "Crafting Boost",
	DUNGEONEERING_MORE_TOKENS_COMBAT_BOOST = "Dungeoneering Boost",
	FARMING_SAVE_SEED = "Farming Boost",
	FIREMAKING_MORE_SPIRITS = "Firemaking Boost",
	FISHING_DOUBLE_CATCH = "Fishing Boost",
	FLETCHING_EXTRA_ITEM = "Fletching Boost",
	HERBLORE_DOUBLE_POTION = "Herblore Boost",
	HUNTER_BOOST_CATCH_CHANCE = "Hunter Boost",
	MINING_DOUBLE_ORE = "Mining Boost",
	RUNECRAFTING_MORE_RUNES = "Runecrafting Boost",
	SMITHING_SAVE_COAL_EXTRA_SMITH = "Smithing Boost",
	THIEVING_INCREASE_THIEF_CHANCE = "Thieving Boost",
	WOODCUTTING_DOUBLE_LOGS_MORE_NESTS = "Woodcutting Boost";


	public static double meleePerk(Player player) {
		if((isMasterOn(player) || player.getBool(MELEE_CRITICAL_STRIKE)) && Utils.random(0, 100) < 3)
			return 2.0;
		return 1.0;
	}

	public static void constitutionPerk(Player player) {return;}

	public static void magicPerk(Player player) {
		return;
	}

	public static void rangePerk(Player player) {
		return;
	}

	public static void slayerPerk(Player player) {
		return;
	}

	public static void summoningPerk(Player player) {
		return;
	}

	public static void agilityPerk(Player player) {
		return;
	}

	public static void constructionPerk(Player player) {
		return;
	}

	public static void cookingPerk(Player player) {
		return;
	}

	public static void craftingPerk(Player player) {
		return;
	}

	public static void dungeoneeringPerk(Player player) {
		return;
	}

	public static void farmingPerk(Player player) {
		return;
	}

	public static void firemakingPerk(Player player) {
		return;
	}

	public static boolean fishingPerk(Player player) {
		if((isMasterOn(player) || player.getBool("FISHING_DOUBLE_CATCH")) && Utils.random(0, 100) < 99) {
			player.sendMessage("You just got double catch");

			return true;
		}
		return false;
	}

	public static void fletchingPerk(Player player) {
		return;
	}

	public static boolean herblorePerk(Player player) {
		if((isMasterOn(player) || player.getBool("HERBLORE_DOUBLE_POTION")) && Utils.random(0, 100) < 5) {
			player.sendMessage("You just got an extra potion");
			return true;
		}
		return false;
	}

	public static void hunterPerk(Player player) {
		return;
	}

	public static void miningPerk(Player player) {
		return;
	}

	public static void runecraftingPerk(Player player) {
		return;
	}

	public static void smithingPerk(Player player) {
		return;
	}

	public static void thievingPerk(Player player) {
		return;
	}

	public static void woodcuttingPerk(Player player) {
		return;
	}
}
