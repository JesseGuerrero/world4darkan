package com.rs.custom.gim;

import com.rs.cache.loaders.ItemDefinitions;
import com.rs.cache.loaders.interfaces.IFEvents;
import com.rs.engine.dialogue.Dialogue;
import com.rs.engine.dialogue.Options;
import com.rs.game.World;
import com.rs.game.content.holidayevents.easter.easter22.Easter2022;
import com.rs.game.content.skills.runecrafting.Runecrafting;
import com.rs.game.content.skills.summoning.Familiar;
import com.rs.game.model.entity.player.Bank;
import com.rs.game.model.entity.player.Equipment;
import com.rs.game.model.entity.player.Player;
import com.rs.game.tasks.WorldTask;
import com.rs.game.tasks.WorldTasks;
import com.rs.lib.game.Item;
import com.rs.lib.net.ServerPacket;
import com.rs.lib.util.Utils;
import com.rs.utils.ItemConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SharedBank {
	private transient List<Player> players = new ArrayList<Player>();
	private Item[][] bankTabs;
	private transient Item[] lastContainerCopy;
	private transient Map<String, Integer> currentTab = new HashMap<>();
	private transient Map<String, Integer> lastX = new HashMap<>();
	private transient Map<String, Boolean> insertItems = new HashMap<>();


	public static final int MAX_BANK_SIZE = 800;

	public SharedBank() {
		bankTabs = new Item[1][0];
	}
	public void open(Player player) {
		if(player.getO("GIM Team") == null) {
			player.sendMessage("You need to be part of a group to access a bank...");
			return;
		}
//		if (!checkPin())
//			return;

		initMaps(player);
		player.getTempAttribs().setO("Shared Bank", this);
		player.getTempAttribs().setO("Bank Name", "Bank of " + Utils.formatPlayerNameForDisplay(GIM.getGIMTeamName(player)));
		openBank(player);
		WorldTasks.schedule(new WorldTask() {
			@Override
			public void run() {
				if(!player.getInterfaceManager().topOpen(762) || player.hasFinished() || player.getTempAttribs().getO("Shared Bank") == null) {
					if(players.contains(player))
						players.remove(player);
					stop();
				}
			}
		}, 10, 5);

		player.setCloseInterfacesEvent(() -> {
			player.getSession().writeToQueue(ServerPacket.TRIGGER_ONDIALOGABORT);
			Familiar.sendLeftClickOption(player);
			if(players.contains(player))
				players.remove(player);
			player.getTempAttribs().setO("Shared Bank", null);
			player.getTempAttribs().setO("Bank Name", null);
		});
	}
	private void openBank(Player player) {
		player.getTempAttribs().removeB("viewingOtherBank");
		player.getVars().setVar(638, 0);
		player.getVars().setVarBit(8348, 0);
		refreshTabs();
		refreshViewingTab();
		refreshLastX();
		refreshTab(currentTab.get(player.getUsername()));
		player.getVars().syncVarsToClient();
		player.getInterfaceManager().sendInterface(762);
		player.getInterfaceManager().sendInventoryInterface(763);
		player.getPackets().sendRunScript(2319);
		if(player.getTempAttribs().getO("Bank Name") != null)
			player.getPackets().setIFText(762, 47, player.getTempAttribs().getO("Bank Name"));
		if(player.getTempAttribs().getO("Bank Name") == null)
			player.getPackets().setIFText(762, 47, "Personal Bank");
		unlockButtons(player);
		sendItems();
		refreshItems();
		player.setCloseInterfacesEvent(() -> {
			player.getSession().writeToQueue(ServerPacket.TRIGGER_ONDIALOGABORT);
			Familiar.sendLeftClickOption(player);
		});
	}

	private void initMaps(Player player) {
		if(!players.contains(player))
			players.add(player);
		if(!currentTab.containsKey(player.getUsername()))
			currentTab.put(player.getUsername(), 0);
		if(!lastX.containsKey(player.getUsername()))
			lastX.put(player.getUsername(), 14);
		if(!insertItems.containsKey(player.getUsername()))
			insertItems.put(player.getUsername(), false);
	}

	public void refreshTabs() {
		for (int slot = 1; slot < 9; slot++)
			refreshTab(slot);
	}

	public void refreshTab(int slot) {
		if (slot == 0)
			return;
		for(Player player : players)
			player.getVars().setVarBit(4885 + (slot - 1), getTabSize(slot));
		refreshViewingTab();
	}

	public int getTabSize(int slot) {
		if (slot >= bankTabs.length)
			return 0;
		return bankTabs[slot].length;
	}
	public void refreshViewingTab() {
		for(Player player : players) {
			player.getVars().setVarBit(4893, currentTab.get(player.getUsername()) + 1);
			player.getVars().syncVarsToClient();
		}
	}

	public void refreshLastX() {
		for(Player player : players)
			player.getVars().setVar(1249, lastX.get(player.getUsername()));
	}

	public void unlockButtons(Player player) {
		player.getPackets().setIFEvents(new IFEvents(762, 95, 0, MAX_BANK_SIZE)
				.enableRightClickOptions(0,1,2,3,4,5,6,9)
				.setDepth(2)
				.enableDrag());
		player.getPackets().setIFEvents(new IFEvents(763, 0, 0, 27)
				.enableUseOptions(IFEvents.UseFlag.ICOMPONENT)
				.enableRightClickOptions(0,1,2,3,4,5,9)
				.setDepth(1)
				.enableDrag());
	}

	public void sendItems() {
		for(Player player : players) {
			player.getPackets().sendItems(95, getContainerCopy());
			player.getPackets().sendVarc(1038, 0);
			player.getPackets().sendVarc(192, getBankSize());
		}
	}

	public Item[] getContainerCopy() {
		if (lastContainerCopy == null)
			lastContainerCopy = generateContainer();
		return lastContainerCopy;
	}

	public Item[] generateContainer() {
		Item[] container = new Item[getBankSize()];
		int count = 0;
		for (int slot = 1; slot < bankTabs.length; slot++) {
			System.arraycopy(bankTabs[slot], 0, container, count, bankTabs[slot].length);
			count += bankTabs[slot].length;
		}
		System.arraycopy(bankTabs[0], 0, container, count, bankTabs[0].length);
		return container;
	}

	public void refreshItems() {
		refreshItems(generateContainer(), getContainerCopy());
	}

	public void refreshItems(Item[] itemsAfter, Item[] itemsBefore) {
		if (itemsBefore.length != itemsAfter.length) {
			lastContainerCopy = itemsAfter;
			sendItems();
			return;
		}
		int[] changedSlots = new int[itemsAfter.length];
		int count = 0;
		for (int index = 0; index < itemsAfter.length; index++)
			if (itemsBefore[index] != itemsAfter[index])
				changedSlots[count++] = index;
		int[] finalChangedSlots = new int[count];
		System.arraycopy(changedSlots, 0, finalChangedSlots, 0, count);
		lastContainerCopy = itemsAfter;
		refreshItems(finalChangedSlots);
	}

	public void refreshItems(int[] slots) {
		for(Player player : players) {
			player.getPackets().sendUpdateItems(95, getContainerCopy(), slots);
			player.getPackets().sendVarc(1038, 0);
			player.getPackets().sendVarc(192, getBankSize());
		}
	}

	public int getBankSize() {
		int size = 0;
		for (Item[] bankTab : bankTabs)
			size += bankTab.length;
		return size;
	}

	public Item getItem(int id) {
		for (Item[] bankTab : bankTabs)
			for (Item item : bankTab)
				if (item.getId() == id)
					return item;
		return null;
	}

	public boolean hasBankSpace() {
		return getBankSize() < MAX_BANK_SIZE;
	}

	public void addItem(Player player, Item item, boolean refresh) {
		addItem(item, currentTab.get(player.getUsername()), refresh);
	}

	public void addItem(Item item, int creationTab, boolean refresh) {
		if (item.getId() == -1)
			return;
		int[] slotInfo = getItemSlot(item.getId());
		if (slotInfo == null || item.getMetaData() != null || bankTabs[slotInfo[0]][slotInfo[1]].getMetaData() != null) {
			if (creationTab >= bankTabs.length)
				creationTab = bankTabs.length - 1;
			if (creationTab < 0) // fixed now, alex
				creationTab = 0;
			int slot = bankTabs[creationTab].length;
			Item[] tab = new Item[slot + 1];
			System.arraycopy(bankTabs[creationTab], 0, tab, 0, slot);
			tab[slot] = item;
			bankTabs[creationTab] = tab;
			if (refresh)
				refreshTab(creationTab);
		} else {
			Item existingItem = bankTabs[slotInfo[0]][slotInfo[1]];
			bankTabs[slotInfo[0]][slotInfo[1]] = new Item(item.getId(), existingItem.getAmount() + item.getAmount(), item.getMetaData());
		}
		if (refresh)
			refreshItems();
	}

	public int[] getItemSlot(int id) {
		for (int tab = 0; tab < bankTabs.length; tab++)
			for (int slot = 0; slot < bankTabs[tab].length; slot++)
				if (bankTabs[tab][slot].getId() == id)
					return new int[] { tab, slot };
		return null;
	}

	public void switchInsertItems(Player player) {
		boolean insertItems = this.insertItems.get(player.getUsername());
		insertItems = !insertItems;
		this.insertItems.put(player.getUsername(), insertItems);
		player.getVars().setVar(305, insertItems ? 1 : 0);
	}

	public void switchWithdrawNotes(Player player) {
		player.getTempAttribs().setB("withdraw_noted_items", !player.getTempAttribs().getB("withdraw_noted_items"));
	}

	public void depositAllInventory(Player player, boolean banking) {
		if (player.getTempAttribs().getB("viewingOtherBank"))
			return;
		if (Bank.MAX_BANK_SIZE - getBankSize() < player.getInventory().getItems().getSize()) {
			player.sendMessage("Not enough space in your bank.");
			return;
		}
		for (int i = 0; i < 28; i++) {
			depositItem(player, i, Integer.MAX_VALUE, true);
			refreshTabs();
			refreshItems();
		}
	}

	public void setLastX(Player player, int lastX) {
		this.lastX.put(player.getUsername(), lastX);
	}

	public void depositItem(Player player, int invSlot, int quantity, boolean refresh) {
		if (player.getTempAttribs().getB("viewingOtherBank") || quantity < 1 || invSlot < 0 || invSlot > 27)
			return;
		Item item = player.getInventory().getItem(invSlot);
		if (item == null)
			return;
		if (item.getMetaData() != null)
			quantity = 1;
		int amt = player.getInventory().getItems().getNumberOf(item);
		if (amt < quantity)
			item = new Item(item.getId(), amt, item.getMetaData());
		else
			item = new Item(item.getId(), quantity, item.getMetaData());
		ItemDefinitions defs = item.getDefinitions();
		int originalId = item.getId();
		if (defs.isNoted() && defs.getCertId() != -1)
			item.setId(defs.getCertId());
		Item bankedItem = getItem(item.getId());
		if (bankedItem != null) {
			if (bankedItem.getAmount() + item.getAmount() <= 0) {
				item.setAmount(Integer.MAX_VALUE - bankedItem.getAmount());
				player.sendMessage("Not enough space in your bank.");
			}
		} else if (!hasBankSpace()) {
			player.sendMessage("Not enough space in your bank.");
			return;
		}
		player.getInventory().deleteItem(invSlot, new Item(originalId, item.getAmount(), item.getMetaData()));
		addItem(player, item, refresh);
	}

	public int addItems(Player player, Item[] items, boolean refresh) {
		int space = MAX_BANK_SIZE - getBankSize();
		if (space != 0) {
			space = (space < items.length ? space : items.length);
			for (int i = 0; i < space; i++) {
				if (items[i] == null)
					continue;
				if (items[i].getId() == Easter2022.EGGSTERMINATOR) {
					player.sendMessage("The banker drops the Eggsterminator as you hand it to them. You can obtain a new one from the Easter event, completing three hunts will unlock a more sturdy enchanted version.");
					continue;
				}
				if (items[i].getDefinitions().isNoted() && items[i].getDefinitions().getCertId() != -1)
					items[i].setId(items[i].getDefinitions().getCertId());
				addItem(player, items[i], false);
			}
			if (refresh) {
				refreshTabs();
				refreshItems();
			}
		}
		return space;
	}

	public boolean removeItem(int[] slot, int quantity, boolean refresh, boolean forceDestroy) {
		if (slot == null)
			return false;
		Item item = bankTabs[slot[0]][slot[1]];
		boolean destroyed = false;
		if (quantity >= item.getAmount()) {
			if (bankTabs[slot[0]].length == 1 && (forceDestroy || bankTabs.length != 1)) {
				destroyTab(slot[0]);
				if (refresh)
					refreshTabs();
				destroyed = true;
			} else {
				Item[] tab = new Item[bankTabs[slot[0]].length - 1];
				System.arraycopy(bankTabs[slot[0]], 0, tab, 0, slot[1]);
				System.arraycopy(bankTabs[slot[0]], slot[1] + 1, tab, slot[1], bankTabs[slot[0]].length - slot[1] - 1);
				bankTabs[slot[0]] = tab;
				if (refresh)
					refreshTab(slot[0]);
			}
		} else
			bankTabs[slot[0]][slot[1]] = new Item(item.getId(), item.getAmount() - quantity, item.getMetaData());
		if (refresh)
			refreshItems();
		return destroyed;
	}

	public void destroyTab(int slot) {
		Item[][] tabs = new Item[bankTabs.length - 1][];
		System.arraycopy(bankTabs, 0, tabs, 0, slot);
		System.arraycopy(bankTabs, slot + 1, tabs, slot, bankTabs.length - slot - 1);
		bankTabs = tabs;
		for(Player player : players) {
			if(player == null)
				continue;
			if (currentTab.get(player.getUsername()) != 0 && currentTab.get(player.getUsername()) >= slot)
				currentTab.put(player.getUsername(), currentTab.get(player.getUsername())-1);
		}
	}

	public int[] getRealSlot(int slot) {
		for (int tab = 1; tab < bankTabs.length; tab++) {
			if (slot < bankTabs[tab].length)
				return new int[] { tab, slot };
			slot -= bankTabs[tab].length;
		}
		if (slot >= bankTabs[0].length)
			return null;
		return new int[] { 0, slot };
	}

	public Item getItem(int[] slot) {
		if (slot == null)
			return null;
		return bankTabs[slot[0]][slot[1]];
	}

	public boolean removeItem(int fakeSlot, int quantity, boolean refresh, boolean forceDestroy) {
		return removeItem(getRealSlot(fakeSlot), quantity, refresh, forceDestroy);
	}

	public void switchItem(Player p, int fromSlot, int toSlot) {
		int[] fromRealSlot = getRealSlot(fromSlot);
		Item fromItem = getItem(fromRealSlot);
		if (fromItem == null)
			return;
		int[] toRealSlot = getRealSlot(toSlot);
		Item toItem = getItem(toRealSlot);
		if (toItem == null)
			return;
		bankTabs[fromRealSlot[0]][fromRealSlot[1]] = toItem;
		bankTabs[toRealSlot[0]][toRealSlot[1]] = fromItem;
		refreshTab(fromRealSlot[0]);
		if (fromRealSlot[0] != toRealSlot[0])
			refreshTab(toRealSlot[0]);
		refreshItems();
	}

	public int getStartSlot(int tabId) {
		int slotId = 0;
		for (int tab = 1; tab < (tabId == 0 ? bankTabs.length : tabId); tab++)
			slotId += bankTabs[tab].length;
		return slotId;
	}

	public void createTab() {
		int slot = bankTabs.length;
		Item[][] tabs = new Item[slot + 1][];
		System.arraycopy(bankTabs, 0, tabs, 0, slot);
		tabs[slot] = new Item[0];
		bankTabs = tabs;
	}

	public void depositLastAmount(Player player, int bankSlot) {
		depositItem(player, bankSlot, lastX.get(player.getUsername()), true);
	}

	public void insert(int fromSlot, int toSlot) {
		int[] fromRealSlot = getRealSlot(fromSlot);
		Item fromItem = getItem(fromRealSlot);
		if (fromItem == null)
			return;

		int[] toRealSlot = getRealSlot(toSlot);
		Item toItem = getItem(toRealSlot);
		if ((toItem == null) || (toRealSlot[0] != fromRealSlot[0]))
			return;

		if (toRealSlot[1] > fromRealSlot[1])
			for (int slot = fromRealSlot[1]; slot < toRealSlot[1]; slot++) {
				Item temp = bankTabs[toRealSlot[0]][slot];
				bankTabs[toRealSlot[0]][slot] = bankTabs[toRealSlot[0]][slot + 1];
				bankTabs[toRealSlot[0]][slot + 1] = temp;
			}
		else if (fromRealSlot[1] > toRealSlot[1])
			for (int slot = fromRealSlot[1]; slot > toRealSlot[1]; slot--) {
				Item temp = bankTabs[toRealSlot[0]][slot];
				bankTabs[toRealSlot[0]][slot] = bankTabs[toRealSlot[0]][slot - 1];
				bankTabs[toRealSlot[0]][slot - 1] = temp;
			}
		refreshItems();
	}

	public void depositAllEquipment(Player player, boolean banking) {
		if (player.getTempAttribs().getB("viewingOtherBank"))
			return;
		for (int i = 0; i < Equipment.SIZE; i++) {
			Item prev = player.getEquipment().setSlot(i, null);
			if (prev == null || prev.getId() == -1)
				continue;
			if (addItems(player, new Item[] { prev }, banking) <= 0) {
				player.sendMessage("Not enough space in your bank.");
				break;
			}
		}
		player.getAppearance().generateAppearanceData();
	}

	public void depositAllBob(Player player, boolean banking) {
		if (player.getTempAttribs().getB("viewingOtherBank"))
			return;
		Familiar familiar = player.getFamiliar();
		if (familiar == null || familiar.getInventory() == null)
			return;
		int space = addItems(player, familiar.getInventory().array(), banking);
		if (space != 0) {
			for (int i = 0; i < space; i++)
				familiar.getInventory().set(i, null);
		}
		if (space < familiar.getInventory().getSize()) {
			player.sendMessage("Not enough space in your bank.");
			return;
		}
	}

	public void setCurrentTab(Player player, int currentTab) {
		if (currentTab >= bankTabs.length)
			return;
		this.currentTab.put(player.getUsername(), currentTab);
		player.getPackets().sendVarc(190, 1);
	}

	public void collapse(Player player, int tabId) {
		if (tabId == 0 || tabId >= bankTabs.length)
			return;
		Item[] items = bankTabs[tabId];
		for (Item item : items)
			removeItem(getItemSlot(item.getId()), item.getAmount(), false, true);
		for (Item item : items)
			addItem(item, 0, false);
		refreshTabs();
		refreshItems();
	}

	public void withdrawItem(Player player, int bankSlot, int quantity) {
		if (player.getTempAttribs().getB("viewingOtherBank") || (quantity < 1))
			return;
		Item item = getItem(getRealSlot(bankSlot));
		if (item == null)
			return;
		if (item.getMetaData() != null)
			quantity = 1;
		if (item.getAmount() < quantity)
			item = new Item(item.getId(), item.getAmount(), item.getMetaData());
		else
			item = new Item(item.getId(), quantity, item.getMetaData());
		boolean noted = false;
		ItemDefinitions defs = item.getDefinitions();
		if (/*withdrawNotes*/player.getTempAttribs().getB("withdraw_noted_items"))
			if (!defs.isNoted() && defs.getCertId() != -1 && item.getMetaData() == null) {
				item.setId(defs.getCertId());
				noted = true;
			} else
				player.sendMessage("You cannot withdraw this item as a note.");
		if ((noted || defs.isStackable()) && item.getMetaData() == null) {
			if (player.getInventory().getItems().containsOne(item)) {
				int slot = player.getInventory().getItems().getThisItemSlot(item);
				Item invItem = player.getInventory().getItems().get(slot);
				if (invItem.getAmount() + item.getAmount() <= 0) {
					item.setAmount(Integer.MAX_VALUE - invItem.getAmount());
					player.sendMessage("Not enough space in your inventory.");
					return;
				}
			} else if (!player.getInventory().hasFreeSlots()) {
				player.sendMessage("Not enough space in your inventory.");
				return;
			}
		} else {
			int freeSlots = player.getInventory().getFreeSlots();
			if (freeSlots == 0) {
				player.sendMessage("Not enough space in your inventory.");
				return;
			}
			if (freeSlots < item.getAmount()) {
				item.setAmount(freeSlots);
				player.sendMessage("Not enough space in your inventory.");
			}
		}
		removeItem(bankSlot, item.getAmount(), true, false);
		player.getInventory().addItem(item);
		if (item.getId() == Runecrafting.RUNE_ESS || item.getId() == Runecrafting.PURE_ESS)
			Runecrafting.fillPouchesFromBank(player, item.getId());
	}

	public void withdrawLastAmount(Player player, int slotId) {
		withdrawItem(player, slotId, lastX.get(player.getUsername()));
	}

	public void withdrawItemButOne(Player player, int fakeSlot) {
		int[] fromRealSlot = getRealSlot(fakeSlot);
		Item item = getItem(fromRealSlot);
		if (item == null)
			return;
		if (item.getAmount() <= 1) {
			player.sendMessage("You only have one of this item in your bank");
			return;
		}
		withdrawItem(player, fakeSlot, item.getAmount() - 1);
	}

	public void sendExamine(Player player, int fakeSlot) {
		int[] slot = getRealSlot(fakeSlot);
		if (slot == null)
			return;
		Item item = bankTabs[slot[0]][slot[1]];
		player.sendMessage(ItemConfig.get(item.getId()).getExamine(item));
		if (item.getMetaData("combatCharges") != null)
			player.sendMessage("<col=FF0000>It looks like it will last another " + Utils.ticksToTime(item.getMetaDataI("combatCharges")));
	}

	public void switchItem(Player player, int fromSlot, int toSlot, int fromComponentId, int toComponentId) {
		if (toSlot == -1) {
			int toTab = toComponentId >= 76 ? 8 - (84 - toComponentId) : 9 - ((toComponentId - 46) / 2);
			if (toTab < 0 || toTab > 9)
				return;
			if (bankTabs.length == toTab) {
				int[] fromRealSlot = getRealSlot(fromSlot);
				if (fromRealSlot == null)
					return;
				if (toTab == fromRealSlot[0]) {
					switchItem(null, fromSlot, getStartSlot(toTab));
					return;
				}
				Item item = getItem(fromRealSlot);
				if (item == null)
					return;
				removeItem(fromSlot, item.getAmount(), false, true);
				createTab();
				bankTabs[bankTabs.length - 1] = new Item[] { item };
				refreshTab(fromRealSlot[0]);
				refreshTab(toTab);
				refreshItems();
			} else if (bankTabs.length > toTab) {
				int[] fromRealSlot = getRealSlot(fromSlot);
				if (fromRealSlot == null)
					return;
				if (toTab == fromRealSlot[0]) {
					switchItem(player, fromSlot, getStartSlot(toTab));
					return;
				}
				Item item = getItem(fromRealSlot);
				if (item == null)
					return;
				boolean removed = removeItem(fromSlot, item.getAmount(), false, true);
				if (!removed)
					refreshTab(fromRealSlot[0]);
				else if (fromRealSlot[0] != 0 && toTab >= fromRealSlot[0])
					toTab -= 1;
				refreshTab(fromRealSlot[0]);
				addItem(item, toTab, true);
			}
		} else if (!insertItems.get(player.getUsername()))
			switchItem(player, fromSlot, toSlot);
		else
			insert(fromSlot, toSlot);
	}
}
