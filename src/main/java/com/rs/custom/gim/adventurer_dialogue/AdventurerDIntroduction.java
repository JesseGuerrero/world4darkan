package com.rs.custom.gim.adventurer_dialogue;

import com.rs.custom.gim.GIM;
import com.rs.custom.gim.GroupIronMan;
import com.rs.db.WorldDB;
import com.rs.engine.dialogue.Conversation;
import com.rs.engine.dialogue.Dialogue;
import com.rs.engine.dialogue.HeadE;
import com.rs.engine.dialogue.Options;
import com.rs.game.World;
import com.rs.game.model.entity.player.Player;
import com.rs.lib.util.Utils;
import com.rs.plugin.annotations.PluginEventHandler;

@PluginEventHandler
public class AdventurerDIntroduction extends Conversation {
	public static int NPC = 1512;
	public AdventurerDIntroduction(Player player) {
		super(player);
		startCreatingGIM();
		create();
	}

	private void startCreatingGIM() {
		addPlayer(HeadE.HAPPY_TALKING, "Hello there.");
		addNPC(NPC, HeadE.CALM_TALK, "Greetings! How may I help you?");
		addOptions("What would you like to do?", option -> {
			if(GIM.hasTeam(player)) {
				option.add("What is my group name & members?", groupInformationDialogue(player));
				if(!GIM.isGroupFounder(player))
					option.add("I would like to leave my group.", leaveGroupDialogue(player));
			}
			if(!GIM.hasTeam(player)) {
				option.add("I'd like to start a new group.", startGroupDialogue(player));
				option.add(" I'd like to join a group.", joinGroupDialogue(player));
			}
			if(GIM.isGroupFounder(player)) {
				if(GIM.getGIMUnsyncReadOnly(player).getSize() <= 7)
					option.add("I'd like to accept someone into my group.", acceptPlayerDialogue(player));
				option.add("Kick a member.", kickMemberDialogue(player));
				option.add("I'd like to disband my group.", disbandGroupDialogue(player));
				option.add("Rename team", renameTeamDialogue(player));
			}
		});
	}

	public static Dialogue groupInformationDialogue(Player player) {
		return new Dialogue()
			.addNPC(NPC, HeadE.CALM_TALK, "Your group name is...")
			.addNPC(NPC, HeadE.CALM_TALK, "\"" + Utils.formatPlayerNameForDisplay(player.getO("GIM Team")) + "\"")
			.addNext(()->{
				GroupIronMan group = World.getGIMMap().get(GIM.getGIMTeamName(player));
				String membersListing = "";
				for(String member : group.getPlayers())
					membersListing = membersListing + "\"" + member + "\"<br>";
				player.startConversation(new Dialogue()
						.addNPC(NPC, HeadE.CALM_TALK, "You group members are...")
						.addNPC(NPC, HeadE.CALM_TALK, membersListing)
				);
			});
	}

	public static Dialogue startGroupDialogue(Player player) {
		return new Dialogue()
				.addNPC(NPC, HeadE.CALM_TALK, "Perfect! What would you like to call your group?")
				.addNext(()->{
							player.sendInputName("What group name would you like?", groupNameDisplay -> {
										if(groupNameDisplay.equals("")) {
											player.sendMessage("Invalid group");
											return;
										}
										groupNameDisplay = Utils.formatPlayerNameForProtocol(groupNameDisplay);
										final String groupName = groupNameDisplay;
										if (!WorldDB.getGIMS().groupExists(groupName)) {
											GroupIronMan group = new GroupIronMan(groupName, player);
											WorldDB.getGIMS().save(group, () -> {
												player.sendMessage("<col=00FF00>You are no longer xp locked...");
												player.save("GIM Team", groupName);
												player.setXpLocked(false);
												player.startConversation(new Dialogue()
														.addNPC(NPC, HeadE.CALM_TALK, "Excellent! I couldn't think of a better one.")
														.addNPC(NPC, HeadE.CALM_TALK, "Scout some players to join you, then speak to me again to include them in your team.")
												);
											});
											World.getGIMMap().put(groupName, group);
											return;
										}
										player.sendMessage("This group name already exists!");
									}
							);
						}
				);
	}

	public static Dialogue acceptPlayerDialogue(Player player) {
		if(GIM.getGIMUnsyncReadOnly(player).getSize() >= 7)
			return new Dialogue().addNPC(NPC, HeadE.CALM_TALK, "Your party is full...");
		return new Dialogue()
				.addPlayer(HeadE.HAPPY_TALKING, "I would like to accept a new member please...")
				.addNext(()->{
					player.sendInputName("What player would you like to accept?", displayname -> {
								Player member = World.getPlayerByDisplay(displayname);
								if (member != null && WorldDB.getGIMS().groupExists(player.getO("GIM Team"))
										&& player.getTempAttribs().getB("GIM Join Request_" +member.getUsername())
										&& member.getTempAttribs().getB("GIM Group Request_"+player.getO("GIM Team"))) {
									GroupIronMan group = World.getGIMMap().get(GIM.getGIMTeamName(player));
									group.addPlayer(member);
									player.sendMessage(member.getDisplayName() + " has been added to the group...");
									member.save("GIM Team", group.getGroupName());
									member.sendMessage("<col=00FFFF>You have joined " + group.getGroupName() + "!</col>");
									member.setXpLocked(false);
									member.sendMessage("<col=00FF00>You are no longer xp locked...");
									return;
								}
								player.sendMessage("Either the player is not logged in or there isn't a request!");
							}
					);
				});
	}

	public static Dialogue disbandGroupDialogue(Player player) {
		return new Dialogue()
				.addNPC(NPC, HeadE.CALM_TALK, "Are you certain?")
				.addOptions("Disband group?", confirm -> {
					confirm.add("No");
					confirm.add("Yes", () -> {
						GIM.removeGIM(GIM.getGIMTeamName(player));
						player.sendMessage("Group disbanded...");
						player.setXpLocked(true);
						player.sendMessage("<col=FF0000><shad=000000>You are XP locked until you are in a team...");
					});
				});
	}

	public static Dialogue joinGroupDialogue(Player player) {
		return new Dialogue()
				.addPlayer(HeadE.HAPPY_TALKING, "I would like to join a group.")
				.addNext(()->{
					player.sendInputName("What group would you like to join?", groupNameDisplay -> {
								if(groupNameDisplay.equals("")) {
									player.sendMessage("Invalid group");
									return;
								}
								groupNameDisplay = Utils.formatPlayerNameForProtocol(groupNameDisplay);
								final String groupName = groupNameDisplay;
								if (World.getGIMMap().containsKey(groupName)) {
									GroupIronMan group = World.getGIMMap().get(groupName);
									if(group.getPlayers().size() > 7) {
										player.sendMessage("This group is full...");
										return;
									}
									Player founder = World.getPlayerByUsername(group.getFounderUsername());
									founder.getTempAttribs().setB("GIM Join Request_"+player.getUsername(), true);
									player.getTempAttribs().setB("GIM Group Request_"+groupName, true);
									group.broadcastMessage("<col=00FFFF>" + player.getDisplayName() + " wishes to join your group...</col>");
									player.startConversation(
											new Dialogue().addNPC(NPC, HeadE.CALM_TALK, "Have the founder talk to me and neither of you log out.")
									);
									return;
								}
								player.sendMessage("This group doesn't exists!");
							}
					);
				});
	}

	public static Dialogue leaveGroupDialogue(Player player) {
		return new Dialogue()
				.addOptions("Are you sure you want to leave the group?", new Options() {
					@Override
					public void create() {
						option("No", new Dialogue());
						option("Yes", new Dialogue()
								.addPlayer(HeadE.CALM_TALK, "Yes I want to leave my group...")
								.addNPC(NPC, HeadE.CALM_TALK, "You will have to be accepted again by the leader")
								.addOptions("Are you sure?", new Options() {
									@Override
									public void create() {
										option("Yes.", new Dialogue()
												.addNext(()->{
													GroupIronMan group = World.getGIMMap().get(GIM.getGIMTeamName(player));
													if(group.getPlayers().size() < 2) {
														player.sendMessage("You can't delete a group entirely!");
														return;
													}
													if(group.isGroupLeader(player)) {
														player.sendMessage("You can't leave your founding group!");
														return;
													}
													int i = group.getPlayers().indexOf(player.getUsername());
													player.delete("GIM Team");
													group.getPlayers().remove(i);
													group.broadcastMessage(player.getDisplayName() + " has left group \"" + group.getGroupName() + "\"");
													player.sendMessage("You left group \"" + group.getGroupName() + "\"");
													player.setXpLocked(true);
													player.sendMessage("<col=FF0000><shad=000000>You are XP locked until you are in a team...");
												})
										);
										option("No.", new Dialogue());
									}
								})
						);
					}
				});
	}

	public static Dialogue kickMemberDialogue(Player player) {
		return new Dialogue()
				.addNext(()->{
					player.sendInputName("What player would you like to kick?", displayname -> {
						if (!WorldDB.getGIMS().groupExists(player.getO("GIM Team"))) {
							player.sendMessage("Your group doesn't exist! This is not expected, contact an admin");
							return;
						}
						World.forceGetPlayerByDisplay(displayname, member -> {
							if(groupNamesMatch(member.getO("GIM Team"), player.getO("GIM Team"))) {
								GroupIronMan group = World.getGIMMap().get(GIM.getGIMTeamName(player));
								if(group.getPlayers().size() < 2) {
									player.sendMessage("You can't delete a group entirely!");
									return;
								}
								if(group.isGroupLeader(member)) {
									player.sendMessage("You can't kick the group leader!");
									return;
								}
								int i = group.getPlayers().indexOf(member.getUsername());
								player.getTempAttribs().setI("kick member", i);
								for(String groupMember : group.getPlayers()) {
									Player p = World.getPlayerByUsername(groupMember);
									p.sendMessage(p.getDisplayName() + " wants to kick " + member.getDisplayName());
									if(p.getUsername() != member.getUsername() && p.getTempAttribs().getI("kick member") != i)
										return;
								}
								member.delete("GIM Team");
								group.getPlayers().remove(member.getUsername());
								group.broadcastMessage(member.getDisplayName() + " has left group \"" + group.getGroupName() + "\"");
								member.sendMessage("You left group \"" + group.getGroupName() + "\"");
								return;
							}
							player.sendMessage(displayname + " is not in the same group as you!");
						});

					});
				});
	}

	public static Dialogue renameTeamDialogue(Player player) {
		return new Dialogue()
				.addPlayer(HeadE.HAPPY_TALKING, "I would like to rename the team...")
				.addNPC(NPC, HeadE.CALM_TALK, "Sure, what would you like?")
				.addNext(()->{
					player.sendInputName("What group name would you like?", groupNameDisplay -> {
						GroupIronMan group = World.getGIMMap().get(GIM.getGIMTeamName(player));
						if(WorldDB.getGIMS().groupExists(groupNameDisplay))
							player.sendMessage("The group name is already taken!");
						else {
							group.setGroupName(groupNameDisplay);
							String groupName = Utils.formatPlayerNameForProtocol(groupNameDisplay);
							player.sendMessage("The group has been renamed to " + Utils.formatPlayerNameForDisplay(groupName));
						}

					});
				});
	}

	private static boolean groupNamesMatch(String group1, String group2) {
		return Utils.formatPlayerNameForProtocol(group1).equals(Utils.formatPlayerNameForProtocol(group2));
	}
}
