package com.rs.custom.gim;

import com.rs.db.WorldDB;
import com.rs.game.World;
import com.rs.game.model.entity.player.Player;
import com.rs.lib.util.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GroupIronMan {
	private String groupName;//This is the key
	private List<String> players = new ArrayList<>();

	private String founder;
	private Map<String, Object> savingAttributes;
	private SharedBank bank = new SharedBank();

	public GroupIronMan(String groupName, Player founder) {
		this.groupName = groupName;
		this.players.add(0, founder.getUsername());
		this.founder = founder.getUsername();
		init();
	}

	public void init() {
		if(founder == null || !getPlayers().contains(founder))
			founder = getPlayers().get(0);
	}

	public String getFounderUsername() {
		return founder;
	}

	public List<String> getPlayers() {
		return players;
	}

	public SharedBank getSharedBank() {
		return bank;
	}


	public String getGroupName() {
		return groupName;
	}

	public String getGroupDisplayName() {
		return Utils.formatPlayerNameForDisplay(getGroupName());
	}

	public int getSize() {
		return players.size();
	}

	public void addPlayer(Player p) {
		players.add(p.getUsername());
	}

	/**
	 *
	 * @param newName
	 * @return true if successful and false if unsuccessful
	 */
	public void setGroupName(final String newName) {
		World.getGIMMap().remove(getGroupName());
		WorldDB.getGIMS().removeSync(this);
		for(String member : getPlayers())
			World.forceGetPlayerByDisplay(member, player -> {
				player.save("GIM Team", newName);
			});
		this.groupName = newName;
		WorldDB.getGIMS().saveSync(this);
		World.getGIMMap().put(newName, this);
	}

	public void broadcastMessage(String message) {
		for(String member : players) {
			Player p = World.getPlayerByUsername(member);
			if(p != null)
				p.sendMessage(message);
		}
	}

	public boolean isGroupLeader(Player p) {
		return founder.equalsIgnoreCase(p.getUsername());
	}

	public void save(String key, Object value) {
		if (savingAttributes == null)
			savingAttributes = new ConcurrentHashMap<>();
		savingAttributes.put(key, value);
	}

	@SuppressWarnings("unchecked")
	public <T> T getO(String name) {
		if (savingAttributes == null)
			savingAttributes = new ConcurrentHashMap<>();
		if (savingAttributes.get(name) == null)
			return null;
		return (T) savingAttributes.get(name);
	}

	public Object get(String key) {
		if (savingAttributes == null)
			savingAttributes = new ConcurrentHashMap<>();
		if (savingAttributes.get(key) != null)
			return savingAttributes.get(key);
		return false;
	}

	public int getI(String key) {
		return getI(key, -1);
	}

	public int getI(String key, int def) {
		Object val = get(key);
		if (val == Boolean.FALSE)
			return def;
		return (int) (val instanceof Integer ? (int) val : (double) val);
	}

	public long getL(String key) {
		return getL(key, -1L);
	}

	public long getL(String key, long def) {
		Object val = get(key);
		if (val == Boolean.FALSE)
			return def;
		return (val instanceof Long ? (long) val : ((Double) val).longValue());
	}

	public boolean getBool(String key) {
		Object val = get(key);
		if (val == Boolean.FALSE)
			return false;
		return (Boolean) val;
	}

	public Map<String, Object> getSavingAttributes() {
		if (savingAttributes == null)
			savingAttributes = new ConcurrentHashMap<>();
		return savingAttributes;
	}

	public void delete(String key) {
		if (savingAttributes == null)
			savingAttributes = new ConcurrentHashMap<>();
		savingAttributes.remove(key);
	}

	public void updateDBGIM() {
		WorldDB.getGIMS().saveSync(this);
	}
}
