package com.rs.db.collection;

import com.google.gson.JsonIOException;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.Indexes;
import com.rs.custom.gim.GroupIronMan;
import com.rs.lib.db.DBItemManager;
import com.rs.lib.file.JsonFileManager;
import com.rs.lib.util.Logger;
import org.bson.Document;

import java.io.IOException;
import java.util.function.Consumer;

import static com.mongodb.client.model.Filters.eq;

public class GroupIronmanManager extends DBItemManager  {
	public GroupIronmanManager() {
		super("GIM");
	}

	@Override
	public void initCollection() {
		getDocs().createIndex(Indexes.text("groupName"));
	}

	public void getByGroupName(String groupName, Consumer<GroupIronMan> func) {
		execute(() -> {
			GroupIronMan group = getGroupSyncName(groupName);
			if(group != null)
				group.init();
			func.accept(group);
		});
	}

	public void save(GroupIronMan group) {
		save(group, null);
	}

	public void save(GroupIronMan group, Runnable done) {
		execute(() -> {
			saveSync(group);
			if (done != null)
				done.run();
		});
	}

	public void saveSync(GroupIronMan group) {
		if(group != null)
			group.init();
		getDocs().findOneAndReplace(eq("groupName", group.getGroupName()), Document.parse(JsonFileManager.toJson(group)), new FindOneAndReplaceOptions().upsert(true));
	}

	public GroupIronMan getGroupSyncName(String groupName) {
		Document accDoc = getDocs().find(eq("groupName", groupName)).first();
		if (accDoc == null)
			return null;
		try {
			GroupIronMan group = JsonFileManager.fromJSONString(JsonFileManager.toJson(accDoc), GroupIronMan.class);
			group.init();
			return group;
		} catch (JsonIOException | IOException e) {
			Logger.handle(this.getClass(), "syncname", e);
			return null;
		}
	}

	public void remove(GroupIronMan group, Runnable done) {
		execute(() -> {
			removeSync(group);
			if (done != null)
				done.run();
		});
	}

	public void removeSync(GroupIronMan group) {
		getDocs().findOneAndDelete(Filters.eq("groupName", group.getGroupName()));
	}

	public boolean groupExists(String groupName) {
		return getGroupSyncName(groupName) != null;
	}

}
